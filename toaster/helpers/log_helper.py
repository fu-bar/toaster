import logging.config
import os
from logging import Logger
from typing import Set

default_logging_configuration_file = f'{os.path.join(os.path.dirname(__file__), "..", "config", "logging.conf")}'
logging.config.fileConfig(os.environ.get('TOASTER_LOGGING_CONF_PATH', default_logging_configuration_file), disable_existing_loggers=False)

logger_names: Set[str] = set()


def get_logger(name: str, level: int = logging.DEBUG) -> Logger:
    logger = logging.getLogger(name=name)
    logger.setLevel(level=level)
    logger_names.add(name)
    return logger


def set_log_level(level: int) -> None:
    loggers = [logging.getLogger(name) for name in logger_names]
    for logger in loggers:
        logger.setLevel(level=level)
