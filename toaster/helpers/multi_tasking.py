import time
from concurrent.futures import ThreadPoolExecutor
from typing import Callable

__MAX_WORKERS = 4

executor = ThreadPoolExecutor(max_workers=__MAX_WORKERS)


def set_max_working_threads(max_workers: int = __MAX_WORKERS):
    global executor
    if max_workers != __MAX_WORKERS:
        executor.shutdown(wait=True, cancel_futures=True)
        executor = ThreadPoolExecutor(max_workers=max_workers)


def wait_until(condition: Callable, interval=0.1, timeout=1, *args):
    start = time.time()
    while not condition(*args) and time.time() - start < timeout:
        time.sleep(interval)
