import os
from concurrent.futures import Future, ALL_COMPLETED, wait
from typing import Iterable, List, Union

import requests

import toaster.helpers.log_helper
from toaster.helpers import multi_tasking
from toaster.structures.exceptions import ToasterException
from toaster.structures.file_info import FileInfo
from toaster.structures.request_result import RequestResult

log = toaster.helpers.log_helper.get_logger(__name__)


def __upload(url: str, file_info: FileInfo) -> RequestResult:
    with open(file_info.path, 'rb') as fd:
        result = RequestResult(url=url, file_info=file_info)
        try:
            log.debug(f'Uploading {file_info.file_name} to {url}')
            response = requests.post(url=url, files={'file': fd, 'filename': file_info.file_name})
            result.response = response
            log.debug(f'Finished uploading {file_info.file_name} to {url}')
        except Exception as e:
            log.error(f'Failed uploading {file_info.file_name} to {url}')
            result.exception = e
            result.ok = False
        result.ok = response.ok
    return result


def __download(url: str) -> RequestResult:
    result = RequestResult(url=url, file_info=FileInfo())
    try:
        log.debug(f'Downloading {url}')
        response = requests.get(url=url)
        result.response = response
        result.ok = response.ok
        if response.ok:
            log.debug(f'Finished downloading {url}')
    except Exception as e:
        log.error(f'Failed downloading {url}')
        result.exception = e
        result.ok = False
    return result


def upload(url: str, file_infos: Iterable[FileInfo]) -> List[RequestResult]:
    upload_futures: List[Future] = list()
    for file_info in file_infos:
        future = multi_tasking.executor.submit(__upload, url=url, file_info=file_info)
        upload_futures.append(future)
    wait(upload_futures, return_when=ALL_COMPLETED)
    results: List[RequestResult] = [future.result() for future in upload_futures]
    return results


def download(urls: Iterable[str], keep_bytes: bool = True, storage_folder: Union[str, None] = None) -> List[RequestResult]:
    download_futures: List[Future] = list()
    results: List[RequestResult] = list()

    def process_download(future_finished: Future):
        result: RequestResult = future_finished.result()
        if not result.ok:
            raise ToasterException(f'Download failed: {result.url}')

        result.file_info.bytes = result.response.content
        result.file_info.file_name = result.url.split('/')[-1]
        if storage_folder is not None:
            local_path = os.path.join(storage_folder, result.file_info.file_name)
            result.file_info.write(local_path)
        if not keep_bytes:
            result.file_info.clear_bytes()
            result.response = None

    for url in urls:
        future = multi_tasking.executor.submit(__download, url=url)
        future.add_done_callback(process_download)
        download_futures.append(future)
    wait(download_futures, return_when=ALL_COMPLETED)

    return results
