import glob
import os
from typing import Iterable, List

from toaster.helpers import log_helper
from toaster.structures.exceptions import ToasterException
from toaster.structures.file_info import FileInfo

log = log_helper.get_logger(__name__)


def read_folder(folder_path: str, extensions: Iterable[str] = ('*',)) -> List[FileInfo]:
    if not os.path.isdir(folder_path):
        raise ToasterException(f'Not a folder: {folder_path}')
    folder_info: List[FileInfo] = list()
    log.debug(f'Reading from folder {folder_path} files with extensions {extensions}')
    file_paths = []
    for extension in extensions:
        file_paths.extend(glob.glob(os.path.join(f"{folder_path}", f'*.{extension}')))
    log.debug(f'Found {len(file_paths)} such files')
    for file_path in file_paths:
        info = FileInfo()
        info.read(file_path=file_path, read_bytes=False)
        folder_info.append(info)
    return folder_info
