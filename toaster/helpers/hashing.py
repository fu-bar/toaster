import hashlib


def md5(byte_array: bytes) -> str:
    hash_md5 = hashlib.md5(byte_array)
    return hash_md5.hexdigest()
