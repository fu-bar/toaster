import time


def timestamp() -> int:
    return round(time.time() * 1000)
