from typing import Union

from pydantic import BaseModel
from requests import Response

from toaster.structures.file_info import FileInfo


class RequestResult(BaseModel):
    file_info: FileInfo
    url: str
    ok: bool = False
    exception: Union[Exception, None] = None
    response: Union[Response, None] = None

    class Config:
        arbitrary_types_allowed = True
