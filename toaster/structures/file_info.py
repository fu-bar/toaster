import os
from typing import Union

from toaster.helpers.hashing import md5
from toaster.structures.exceptions import ToasterException


class FileInfo:

    def __init__(self,
                 path: str = '',
                 file_name: str = '',
                 hash_code: str = '',
                 size_bytes: int = -1,
                 byte_array: Union[bytes, None] = None):

        self.__path: str = path
        self.__file_name: str = file_name
        self.__hash_code: str = hash_code
        self.__size_bytes: int = size_bytes
        self.__bytes: Union[bytes, None] = byte_array

    @property
    def bytes(self):
        return self.__bytes

    @bytes.setter
    def bytes(self, byte_array: bytes):
        self.__bytes = byte_array
        if byte_array is None:
            self.__size_bytes = -1
            self.__hash_code = ''
        else:
            self.__size_bytes = len(self.__bytes)
            self.__hash_code = md5(byte_array=self.__bytes)

    def read(self, file_path: str, read_bytes=True) -> None:
        if not os.path.isfile(file_path):
            raise ToasterException(f'Not a file: {file_path}')
        self.__path = file_path
        self.__file_name = os.path.basename(file_path)
        self.__size_bytes = os.path.getsize(file_path)
        if read_bytes:
            self.read_bytes()

    def write(self, file_path: str) -> None:
        with open(file_path, 'wb') as f:
            f.write(self.__bytes)
            self.path = file_path

    def read_bytes(self) -> None:
        if not os.path.isfile(self.__path):
            raise ToasterException(f'Can not read bytes from: {self.__path}')
        with open(self.__path, 'rb') as f:
            self.bytes = f.read()

    def clear_bytes(self) -> None:
        self.__bytes = None

    @property
    def path(self):
        return self.__path

    @path.setter
    def path(self, value: str):
        self.__path = value
        self.file_name = os.path.basename(self.path)

    @property
    def file_name(self):
        return self.__file_name

    @file_name.setter
    def file_name(self, value: str):
        self.__file_name = value

    @property
    def hash_code(self):
        return self.__hash_code

    @property
    def size_bytes(self):
        return self.__size_bytes
