import random
import string
from abc import ABC, abstractmethod
from concurrent.futures import ThreadPoolExecutor, Future, wait
from typing import Union, Dict, Any, Set

from toaster.helpers import log_helper
from toaster.helpers.multi_tasking import wait_until
from toaster.helpers.stamping import timestamp
from toaster.structures.exceptions import ToasterException


class TestStep(ABC):
    __executor = ThreadPoolExecutor(max_workers=1)

    def __init__(self, name: str, is_mandatory: bool = True):
        self.__name = name
        self.__uid = ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))
        self.__ok: bool = False
        self.__duration_milliseconds: Union[int, None] = None
        self.__is_mandatory = is_mandatory
        self.__start_timestamp: Union[int, None] = None
        self.__finish_timestamp: Union[int, None] = None
        self.__results: Dict[str, Any] = dict()
        self.__future: Union[Future, None] = None
        self.__parameters: Dict[str, Any] = dict()
        self.__exception: Union[Exception, None] = None
        self.__required_parameters: Set[str] = set()
        self.log = log_helper.get_logger(f'{self.__name}::{self.__uid}]')

    # region Properties
    @property
    def is_mandatory(self) -> bool:
        return self.__is_mandatory

    @property
    def name(self) -> str:
        return self.__name

    @property
    def results(self) -> Dict[str, Any]:
        return self.__results

    @property
    def parameters(self) -> Dict[str, Any]:
        return self.__parameters.copy()

    @property
    def uid(self) -> str:
        return self.__uid

    @uid.setter
    def uid(self, unique_value: str):
        self.__uid = unique_value

    @property
    def exception(self) -> Union[Exception, None]:
        return self.__exception

    @property
    def ok(self) -> bool:
        return self.__ok

    @property
    def error_message(self) -> Union[str, None]:
        if self.exception is None:
            return None
        return str(self.exception)

    @property
    def is_finished(self) -> bool:
        return self.__finish_timestamp is not None

    @property
    def time_elapsed_milli(self) -> int:
        if self.is_finished:
            return self.__duration_milliseconds
        return timestamp() - self.__start_timestamp

    # endregion

    def finish_callback(self, finished_future: Future):
        self.__finish_timestamp = timestamp()
        exception: Union[Exception, None] = finished_future.exception()
        duration_milliseconds = self.__finish_timestamp - self.__start_timestamp
        if exception is not None:
            self.log.error(f'Step {self.name} finished with an exception: {self.exception} after {duration_milliseconds} milliseconds')
            self.__ok = False
            self.__exception = exception
            self.__duration_milliseconds = duration_milliseconds
        else:
            self.log.info(f'Step {self.name} finished successfully in {duration_milliseconds} milliseconds')
            self.__ok = True
            self.__exception = None
            self.__duration_milliseconds = duration_milliseconds

    def add_parameters(self, parameters: Dict[str, Any]):
        self.__parameters = {**self.__parameters, **parameters}

    def get_parameter(self, key):
        if key not in self.parameters.keys():
            raise ToasterException(f'Key {key} is not in available parameters ({self.parameters.keys()})')
        return self.parameters[key]

    def run(self):
        if self.is_finished:
            raise ToasterException(f'Step {self.name} can not be run because it is already finished')
        self.__start_timestamp = timestamp()
        self.__future = TestStep.__executor.submit(self.logic)
        self.log.info(f'Step submitted for execution: {self.name}')
        self.__future.add_done_callback(self.finish_callback)

    def wait_to_finish(self):
        wait([self.__future])
        wait_until(lambda: self.__duration_milliseconds is not None)

    def abort(self):
        self.log.warning(f'Stage {self.name} is being aborted')
        self.__finish_timestamp = timestamp()
        if self.__future is not None:
            self.__future.cancel()
        self.__ok = False
        self.__duration_milliseconds = -1 if self.__start_timestamp is None else self.__finish_timestamp - self.__start_timestamp
        self.__exception = None

    @abstractmethod
    def logic(self) -> None:
        pass

    @abstractmethod
    def required_parameters(self) -> Set[str]:
        pass

    def __hash__(self):
        return hash(f'{self.name}_{self.uid}')

    def __eq__(self, other):
        return self.uid == other.uid
