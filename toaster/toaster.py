from typing import List, Dict, Set

from toaster.helpers import log_helper
from toaster.helpers.stamping import timestamp
from toaster.structures.exceptions import ToasterException
from toaster.structures.test_step import TestStep

log = log_helper.get_logger(__name__)


def run_steps(steps: List[TestStep], execution_tag: str = str(timestamp())):
    log.info(f'Tag: {execution_tag}')

    id_to_step: Dict[str, TestStep] = dict()
    for step in steps:
        id_to_step[step.uid] = step

    log.info(f'Executing {len(steps)} steps: {",".join([step.name for step in steps])}')
    for index, step in enumerate(steps):
        log.debug(f'Running step: {step.name}')
        existing_parameters = set(step.parameters.keys())
        required_parameters = step.required_parameters()
        missing_parameters = required_parameters.difference(existing_parameters)
        found_parameters: Set[str] = set()
        for i in range(index - 1, -1, -1):
            for missing in missing_parameters:
                if missing in found_parameters:
                    continue
                if missing in steps[i].results.keys():
                    step.add_parameters({missing: steps[i].results[missing]})
                    found_parameters.add(missing)
        missing_parameters = missing_parameters.difference(found_parameters)
        if len(missing_parameters) > 0:
            raise ToasterException(f'Step {step.name} is missing parameters: {missing_parameters}')

        step.run()
        step.wait_to_finish()
        if not step.ok and step.is_mandatory:
            log.warning(f'Failure in step {step.name} - ABORTING all following steps!')
            index += 1
            while index < len(steps):
                steps[index].abort()
                index += 1
            break
        log.debug(f'Finished running step: {step.name}')
    if all([step.ok for step in steps]):
        log.info("All steps are OK")
    else:
        not_ok_steps = [step for step in steps if not step.ok]
        log.error(f"The following {len(not_ok_steps)} steps are NOT OK: {','.join([step.name for step in not_ok_steps])}")
