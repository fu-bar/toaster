<div align="center" style="text-align:center; font-weight: bold; font-size: 25px">
<img src="./resources/toaster.png" alt="Toaster by Boris Milner" width="200"/><br>
Toaster, by Boris Milner
</div>

### What is Toaster?

A simple yet powerful pipelining framework made originally for testing (hence the name toaster.).  
Simplify and modularize your tests by creating test-steps (building blocks).

#### Steps are very simple to write

```python
from toaster.structures.test_step import TestStep


class SampleStep(TestStep):
    def required_parameters(self) -> Set[str]:
        return set()

    def logic(self) -> None:
        self.results['first'] = 1
        self.results['second'] = 2
```

#### Running the steps is simple as well

```python
all_steps = [step1, step2, step3]
toaster.run_steps(steps=all_steps)

assert all(step.ok for step in all_steps)
```

#### Isolation & cooperation

All steps are completely self-contained and do not require other steps to run.

Every step implements the ```required_parameters``` (see above).  
Toaster runner automatically supplies each steps with its missing parameters using the results of finished steps.

Setting step-parameters manually, lets you mock and setup any required state, fast and trivially easy.