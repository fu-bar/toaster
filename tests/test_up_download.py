import os
from typing import List

import toaster.helpers.file_system as file_system
import toaster.helpers.up_download as up_download
from toaster.structures.request_result import RequestResult


def test_upload_files():
    file_infos = file_system.read_folder(os.path.dirname(os.path.dirname(__file__)))
    results = up_download.upload(url='http://localhost:8080/v1/payload_files/crap', file_infos=file_infos)


def test_download_files():
    results: List[RequestResult] = up_download.download(urls=['http://localhost:8080/v1/results_files/crap/crap.txt'])
