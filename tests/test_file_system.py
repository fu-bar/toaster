import os.path

import pytest

import toaster.helpers.file_system as file_system
from toaster.structures.exceptions import ToasterException


def test_read_folder():
    folder_info = file_system.read_folder(folder_path=os.path.dirname(__file__))
    assert isinstance(folder_info, list)
    assert len(folder_info) > 1
    assert any(info.file_name == os.path.basename(__file__) for info in folder_info)
    assert any(info.file_name == '__init__.py' for info in folder_info)


def test_read_folder_specific_extensions():
    folder_info = file_system.read_folder(folder_path=os.path.dirname(os.path.dirname(__file__)), extensions=['toml'])
    assert len(folder_info) == 1
    folder_info = file_system.read_folder(folder_path=os.path.dirname(os.path.dirname(__file__)), extensions=['lock'])
    assert len(folder_info) == 1
    folder_info = file_system.read_folder(folder_path=os.path.dirname(os.path.dirname(__file__)), extensions=[])
    assert len(folder_info) == 0
    folder_info = file_system.read_folder(folder_path=os.path.dirname(os.path.dirname(__file__)))
    assert len(folder_info) > 1


def test_bad_read_missing_folder():
    with pytest.raises(ToasterException):
        file_system.read_folder(folder_path=os.path.dirname(__file__) + 'wtf')
