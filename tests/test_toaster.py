from typing import Set

from toaster import toaster
from toaster.structures.test_step import TestStep


class Step1(TestStep):
    def required_parameters(self) -> Set[str]:
        return set()

    def logic(self) -> None:
        self.results['first'] = 1
        self.results['second'] = 2


class Step2(TestStep):
    def required_parameters(self) -> Set[str]:
        return set()

    def logic(self) -> None:
        self.results['third'] = 900
        self.results['fourth'] = 800


class Step3(TestStep):
    def required_parameters(self) -> Set[str]:
        return {'first', 'second', 'third', 'fourth'}

    def logic(self) -> None:
        self.results['first'] = 10 * self.parameters['first']
        self.results['second'] = 10 * self.parameters['second']
        self.results['third'] = self.parameters['third']
        self.results['fourth'] = self.parameters['fourth']


def test_running_steps():
    step1 = Step1(name='First')
    step2 = Step2(name='Second')
    step3 = Step3(name='Third')

    all_steps = [step1, step2, step3]
    toaster.run_steps(steps=all_steps)

    assert all(step.ok for step in all_steps)
    assert all(key in step3.results for key in ['first', 'second', 'third', 'fourth'])
    assert step3.results['first'] == 10
    assert step3.results['second'] == 20
