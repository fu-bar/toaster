from typing import Dict, Any, Set

import pytest

from toaster import toaster
from toaster.structures.exceptions import ToasterException
from toaster.structures.test_step import TestStep


class StepA(TestStep):

    def required_parameters(self) -> Set[str]:
        return set()

    def logic(self) -> None:
        result: Dict[str, Any] = dict()
        for i in range(0, 10):
            result[str(i)] = f'A-{i}'
        self.results.update(result)


def test_running_steps():
    step_a = StepA(name='Step-A', is_mandatory=True)
    steps = [step_a]
    toaster.run_steps(steps=steps, execution_tag='Step-A')
    assert step_a.ok
    for i in range(0, 10):
        assert step_a.results[str(i)] == f'A-{i}'

    with pytest.raises(ToasterException):
        toaster.run_steps(steps=steps, execution_tag="Should fail due to finished step")
