import pytest

from toaster.structures.exceptions import ToasterException
from toaster.structures.file_info import FileInfo


@pytest.mark.parametrize('read_bytes', [True, False])
def test_initialization(read_bytes):
    info = FileInfo()
    info.read(file_path=__file__, read_bytes=read_bytes)
    assert info.file_name == 'test_file_info.py'
    assert info.size_bytes > 100
    assert info.path == __file__
    if read_bytes:
        with open(__file__, 'rb') as f:
            self_bytes = f.read()
        assert info.bytes == self_bytes
        assert info.hash_code != ''
        info.clear_bytes()
        assert info.bytes is None
    else:
        assert info.bytes is None
        assert info.hash_code == ''


def test_bad_non_existing_path():
    info = FileInfo()
    with pytest.raises(ToasterException):
        info.read(file_path='/tmp/does_not_exist.wtf')
